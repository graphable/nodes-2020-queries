// COSINE SIMILARITY QUERY
match(b:Beer{name:'Spandauer Weizen'})-[:HAS_A]->(r:Review)
//FIND TARGET BEER
match(b)-[:IS_A]->(s:Style)
//GET BEER STYLE
with s, b.name as source_beer, avg(r.aroma) as aroma, avg(r.palate) as palate, avg(r.appearance) as appearance, avg(r.taste) as taste
//AVERAGE REVIEW SCORES ACROSS USERS WHO HAVE REVIEWED THE BEER -- FOR AGGREGATION PURPOSES
with s, source_beer, collect([aroma, palate, appearance, taste]) as beer1
unwind beer1 as beer1vec
// MAKE REVIEW SCORES INTO A VECTOR
match(b2:Beer)-[:HAS_A]->(r2:Review)
match(b2)-[:IS_A]->(s2:Style)
// FIND OTHER BEERS OUT THERE 
where b2.name <> source_beer and s<>s2
// MAKE SURE THAT WE FIND DIFFERENT BEERS FROM NEW STYLES
with source_beer, beer1vec, b2.name as target_beer, avg(r2.aroma) as aroma, avg(r2.palate) as palate, avg(r2.appearance) as appearance, avg(r2.taste) as taste
with source_beer, beer1vec, target_beer, collect([aroma, palate, appearance, taste]) as beer2
unwind beer2 as beer2vec
// GO THROUGH SAME VECTOR BUILDING EXERCISE
with source_beer, target_beer, gds.alpha.similarity.cosine(beer1vec, beer2vec) AS similarity
// CALCULATE COSINE SIMILARITY
order by similarity desc limit 10
// PICK TOP 10 CLOSEST BEERS
match(b1:Beer{name:source_beer})-[:IS_A]->(s1:Style)
match(b2:Beer{name:target_beer})-[:IS_A]->(s2:Style)
with b1, b2, similarity, s1, s2, rand() as random
order by random desc limit 1 // RANDOMLY PICK FINAL BEER
return b1.name, b2.name, s1.name, s2.name, similarity
