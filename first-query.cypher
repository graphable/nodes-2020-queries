// BLEND REVIEW TEXT AND BEER NAMES TO FIND MATCHES OF BEERS THEY HAVEN'T REVIEWED
match(s:Style)<-[:IS_A]-(b:Beer)-[:HAS_A]->(r:Review)<-[:WROTE]-(u:User{name:"mikedrinksbeer2"})
// PICK A USER 
with u, s, r, split(toLower(s.name), ' ') as styles // SPLIT STYLES TO GET INDIVIDUAL WORDS
unwind styles as style
with u, s, r, style
where style <> '/' and not style contains '('
// GET RID OF WEIRD CHARACTERS
with u.name as source_user, style, avg(r.aroma) as aroma,  avg(r.appearance) as appearance, avg(r.palate) as palate,  avg(r.overall) as overall
with source_user, style, aroma + appearance + palate + overall as total_score
order by total_score desc
with source_user, collect({user:source_user, style:style, total_score:total_score}) as source_map
with source_user, source_map[0..9] as source_map //GET THE TOP 10 STYLE WORDS
unwind source_map as sourcemaps
match(s:Style)
with source_user,sourcemaps, s.name as style, split(toLower(s.name), ' ') as styles
unwind styles as allstyles
with source_user,sourcemaps,style, allstyles
where allstyles = sourcemaps.style
match(r:Review)<-[:WROTE]-(u:User{name:source_user})
with source_user, style, r, split(r.text, ' ') as text_splits // SPLIT TEXT REVIEWS INTO WORDS
unwind text_splits as review_text
with source_user, style, r, replace(toLower(trim(review_text)),'.','') as review_text // NORMALIZE WORDS
with source_user, style, r, replace(review_text,',','') as review_text
where not review_text contains '('
with source_user, style, review_text, sum(r.aroma) + sum(r.appearance) + sum(r.palate) + sum(r.overall) as review_score
order by review_score desc 
limit 100 // PICK TOP 10 HIGHEST SCORING REVIEW WORDS
with source_user, style, collect({review_text:review_text, review_score:review_score}) as review_map
match(b:Beer)-[:IS_A]->(s:Style{name:style})
with source_user, review_map, s, b, split(b.name, ' ') as name_split
unwind name_split as names
unwind review_map as review
with source_user, s, b, review, toLower(names) as names
where review.review_text = names //FIND BEERS WITH NAMES CONTAINING TEXT FROM THE REVIEWS
with source_user, s, b, avg(review.review_score)/1000 as score
order by score desc
with source_user, s.name as style_name,collect({beer:b.name, score:score}) as beer_map
with source_user, style_name, beer_map[0] as beer_map // PICK TOP BEER FROM EACH STYLE
unwind beer_map as beers
return source_user, style_name, beers.beer as beer, beers.score as score
