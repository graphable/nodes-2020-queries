
// USE BEER NAMES TO MAKE UP BREWER NAMES
match(b:Beer)--(br:Brewer)
// WHEN IN DOUBT MAKE MORE DIMENSIONS
with br, b, split(b.name, " ")[0] as names
unwind names as words
with br, b, words
// REMOVE STOPWORDS AND SPECIAL CHARACTERS
where not words contains '(' and not words contains '&' and words <>'-' and not words contains ')' and not words in ["Beer", 'Ale', 'Stout', 'IPA', 'The', 'Pale', 'And'] and words <> '/'
// GET SOME STATS ON BEERS
with br, b, collect(words) as words
with br, words[0] as first, count(distinct b) as beers
order by beers desc
with distinct br.brewer_id as brewers, collect({first:first, first_beers:beers}) as firsts
with brewers, firsts[0] as top
match(b:Beer)--(br:Brewer{brewer_id:brewers})
// WHEN IN DOUBT MAKE MORE DIMENSIONS
with top, br, b, split(b.name, " ")[1] as names
unwind names as words
with top, br, b, words
// REMOVE STOPWORDS AND SPECIAL CHARACTERS
where not words contains '(' and not words contains '&' and words <>'-' and not words contains ')' and not words in ["Beer", 'Ale', 'Stout', 'IPA', 'The', 'Pale'] and words <> '/'
// GET SOME STATS ON BEERS
with top, br, b, collect(words) as words
with top, br, words[0] as second, count(distinct b) as beers
order by beers desc
with distinct top, br.brewer_id as brewers, collect({second:second, second_beers:beers}) as seconds
with top, brewers, seconds[0] as top2
match(b:Beer)--(br:Brewer{brewer_id:brewers})
// WHEN IN DOUBT MAKE MORE DIMENSIONS
with top, top2, br, b, split(b.name, " ")[2] as names
unwind names as words
with top, top2, br, b, words
// REMOVE STOPWORDS AND SPECIAL CHARACTERS
where not words contains '(' and not words contains '&' and words <>'-' and not words contains ')' and words <> '/'
// GET SOME STATS ON BEERS
with top, top2, br, b, collect(case when words is null then '' else words end) as words
with top, top2, br, words[0] as third, count(distinct b) as beers
order by beers desc
with distinct top, top2, br.brewer_id as brewers, collect({third:third, third_beers:beers}) as thirds
with top, top2, brewers, thirds[0] as top3
unwind top as tops
unwind top2 as tops2
unwind top3 as tops3
with distinct brewers, case when abs(toFloat(tops.first_beers) - toFloat(tops2.second_beers))/toFloat(tops.first_beers) < 0.25 and abs(toFloat(tops2.second_beers) - toFloat(tops3.third_beers))/toFloat(tops2.second_beers) < 0.3 then tops.first + ' ' + tops2.second + ' ' + tops3.third  when tops.first_beers = tops2.second_beers then tops.first + ' ' + tops2.second when (toFloat(tops.first_beers) - toFloat(tops2.second_beers))/toFloat(tops.first_beers) <= .2 then tops.first + ' ' + tops2.second when (toFloat(tops.first_beers) - toFloat(tops2.second_beers))/toFloat(tops.first_beers) <= -0.2 then tops2.second + ' ' + tops.first
when (toFloat(tops.first_beers) - toFloat(tops2.second_beers))/toFloat(tops.first_beers) < -0.2 then tops2.second when (toFloat(tops.first_beers) - toFloat(tops2.second_beers))/toFloat(tops.first_beers) > .4 then tops.first else tops.first + ' ' + tops2.second end as brewer_name
return brewers, brewer_name
